# contact_DerAndere

If you want to report a bug or have a feature request, please use the issue tracker of the specific software the issue is about. A list of the software DerAndere contributed to can be found on the [profile DerAndere at https://gitlab.com](https://gitlab.com/users/DerAndere/groups) and on the [profile DerAndere1 at https://github.com](https://github.com/DerAndere1?tab=repositories). If you have a genearal question or remark, you can contact me ([DerAndere](https://gitlab.com/DerAndere) by creating a new issue at https://gitlab.com/contact_DerAndere/contact_DerAndere/issues. 
You may attach files to comments by dragging & dropping, selecting them, or pasting from the clipboard.

Reminder: By default, your issues, comments and attachments will be public. 

To exchange private information such as email adresses, mark the issue as confidential. Confidential issues cannot be viewed 
by third parties (people other than you and me).
